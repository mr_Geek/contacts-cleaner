import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {

  constructor(
    private toastCtrl: ToastController
  ) {
  }

  toastHandler(message, duration?, closeBtnFlag?, closeBtnText?) {
    return this.toastCtrl.create({
      message: message, 
      duration: duration ? duration : 3000,
      showCloseButton: closeBtnFlag ? closeBtnFlag : false,
      closeButtonText: closeBtnText ? closeBtnText : ''
    })
  }

}
