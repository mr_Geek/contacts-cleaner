import { Component } from '@angular/core';
import { Platform, NavController, AlertController } from 'ionic-angular';
import { Contacts, Contact, ContactField, ContactName, ContactFindOptions } from '@ionic-native/contacts';
import { Diagnostic } from '@ionic-native/diagnostic';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})    
export class HomePage {
  
  public allContacts: Contact[];
  public contactsSize: number;
  public status: string = '';
  public btnDisabled: boolean = false;

  constructor(
    public navCtrl: NavController,
    private contacts: Contacts,
    private diagnostic: Diagnostic,
    private alertCtrl: AlertController,
    private globalProvider: GlobalProvider,
    private platform: Platform
  ) {
    // this.platform.ready().then(() => {
    //   this.status += '-- getting phone contacts.. <br>'

    //   this.contacts.find([
    //     'name',
    //     'phoneNumbers',
    //   ], {
    //     filter: '',
    //     multiple: true
    //   }).then(data => {
    //     this.btnDisabled = false
    //     this.allContacts = data
    //     this.contactsSize = data.length
    //     this.status += '-- got all contacts, you have ' + this.contactsSize + ' contact in your phone<br>'
    //     this.status += '<span class="imp-text">-- now click on the CLEAN button to start cleaning</span><br>'
    //   }).catch(err => {
    //     console.log(err)
    //     globalProvider.toastHandler(
    //       'Error occurred while getting your contacts', 2000, true, 'close'
    //     )
    //   }) 
    // })

    

  }

  cleanProcess() {
    this.status += '-- starting cleaning process<br>\
                    -- finding duplicate numbers<br>'



    this.contacts.find(['name', 'phoneNumbers']).then(data => {
      this.status += '++ GOT CONTACTS<br>'

      
      data.forEach(contact => {
        
        console.log(contact.name.formatted, contact.phoneNumbers)

        
        // if (contact.phoneNumbers.length > 1){

        //   this.status += '(FIXING) contact: ' + contact.name.formatted + '<br>'

        //   var numbersValues = contact.phoneNumbers.map(num => {
        //     return num.value.replace(/[^0-9\.]/g, '')
        //   })

        //   numbersValues.some( (value, index) => {
        //     if (numbersValues.indexOf(value) != index){

        //       contact.phoneNumbers.splice(numbersValues.indexOf(value, 1))
        //       contact.save()
              
        //     }
        //     return numbersValues.indexOf(value) != index
        //   })


        // }
      })





    }).catch(err => {
      this.globalProvider.toastHandler(
        'Error occurred while getting your contacts', 2000, true, 'close'
      )
    }) 
    


    // for (var contact of this.allContacts ) {
      

    //   if (contact.phoneNumbers.length > 1){
    //     var numbersValues = contact.phoneNumbers.map(num => {
    //       return num.value.replace(/[^0-9\.]/g, '')
    //     })
    //     var isDuplicate = numbersValues.some( (value, index) => {
    //       if (numbersValues.indexOf(value) != index) 
    //         contact.phoneNumbers.splice(numbersValues.indexOf(value, 1))

    //       return numbersValues.indexOf(value) != index
    //     })



    //     // if (contact.phoneNumbers[0].value.replace(/[^0-9\.]/g, '') === '970599636118')
    //     //   break
    //   }// End if

    // } // End for

    
  }

}
