import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';

import { GlobalProvider } from '../providers/global/global';
import { Diagnostic } from '@ionic-native/diagnostic';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    diagnostic: Diagnostic,
    alertCtrl: AlertController,
    globalProvider: GlobalProvider
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      diagnostic.getContactsAuthorizationStatus().then(res => {
        console.log('res: ' + res)
      }, err => {
        console.log('err: ' + err)
      })

    });
  }
}
